#!/usr/bin/perl -w
# -*- perl -*-

=head1 NAME

asterix_sipchannels - Plugin to monitor number of active SIP
channels by codec used

=head1 CONFIGURATION

The following configuration parameters are used by this plugin

 [asterisk_sipchannels]
  env.host     - hostname to connect to
  env.port     - port number to connect to
  env.username - username used for authentication
  env.secret   - secret used for authentication
  env.codecs   - List of codecs (names)
  env.codecsx  - List of codecs (hexadecimal values)

The values of codecs and codecx must match each other.  Asterisk
returns a hexadecimal value representing the codec used, and the same
list entry is used from "codecs" and "codecsx".

The "username" and "secret" parameters are mandatory, and have no
defaults.

=head2 DEFAULT CONFIGURATION

 [asterisk_sipchannels]
  env.host 127.0.0.1
  env.port 5038
  env.codecs gsm ulaw alaw
  env.codecsx 0x2 0x4 0x8

=head1 AUTHOR

Copyright (C) 2005-2006 Rodolphe Quiedeville <rodolphe@quiedeville.org>
Update for asterisk >= 14 : Copyright (C) 2016 Etienne Lessard <elessard@proformatique.com>

=head1 LICENSE

Gnu GPLv2

=begin comment

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 2 dated June, 1991.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

If you improve this script please send your version to my email
address with the copyright notice upgrade with your name.

=end comment

=head1 MAGIC MARKERS

 #%# family=contrib

=cut

use strict;

my $ret = undef;
if (! eval "require Net::Telnet;")
{
    $ret = "Net::Telnet not found";
}

#Codecs g723, gsm, ulaw, alaw, g726, adpcm, slin, lpc10, g729, speex, ilbc

my @CODECS = exists $ENV{'codecs'} ? split ' ',$ENV{'codecs'} : qw(gsm ulaw alaw);
my @CODECSX = exists $ENV{'codecsx'} ? split ' ',$ENV{'codecsx'} : qw(0x2 0x4 0x8);

if ($ARGV[0] and $ARGV[0] eq "config")
{
    print "graph_title Asterisk sip channels/codecs\n";
    print "graph_args --base 1000 -l 0\n";
    print "graph_vlabel channels\n";
    print "graph_category asterisk\n";
    foreach my $codec (@CODECS) {
    if ($codec eq $CODECS[0]) {
        print "$codec.draw AREA\n";
    }
    else{
        print "$codec.draw STACK\n";
    }
    print "$codec.label $codec\n";
    }
    print "other.draw STACK\n";
    print "other.label other\n";
    print "unknown.draw STACK\n";
    print "unknown.label not set\n";
    exit 0;
}

my $host = exists $ENV{'host'} ? $ENV{'host'} : "127.0.0.1";
my $port = exists $ENV{'port'} ? $ENV{'port'} : "5038";

my $username = $ENV{'username'};
my $secret   = $ENV{'secret'};

my $pop = new Net::Telnet (Telnetmode => 0);
$pop->open(Host => $host,
       Port => $port);

## Read connection message.
my $line = $pop->getline;
die $line unless $line =~ /^Asterisk/;

## Send user name.
$pop->print("Action: login");
$pop->print("Username: $username");
$pop->print("Secret: $secret");
$pop->print("Events: off");
$pop->print("");

#Response: Success
while (($line = $pop->getline) and ($line ne "\n"))
{}

## Request status of messages.
$pop->print("Action: command");
$pop->print("Command: pjsip show channelstats");
$pop->print("");

#Message: Command output follows
#Output:
#Output:                                              ...........Receive......... .........Transmit..........
#Output:  BridgeId ChannelId ........ UpTime.. Codec.   Count    Lost Pct  Jitter   Count    Lost Pct  Jitter RTT....
#Output:  ===========================================================================================================
#Output:
#Output:  1fd434c9 bpxiqzle-00000000  00:00:13 ulaw      537       0    0   0.000    499       0    0   0.005   0.001
#Output:  1fd434c9 ow7q6dur-00000001  00:00:10 ulaw      400       0    0   0.000    418       0    0   0.000   0.007
#Output:
#Output: Objects found: 2
#Output:

my @results;
my ($i, $start, $unknown, $other, $fields)=(0,0,0,0,0);
foreach my $codec (@CODECS) {
    $results[$i] = 0;
    $i++;
}

# When no channels active PJSIP prints 'No objects found'
while (($line = $pop->getline) and ($line !~ /bjects found/o))
{
    $i = 0;
    if ($start) {
        $line =~ s/^Output: //;
        next if ($line eq "\n");
        my @fields = (split ' ', $line);
        my $chan_codec = $fields[3];
        if ($chan_codec eq 'nothing') {
            $unknown += 1;
            next;
        }
        my $found = 0;
        foreach my $codec (@CODECS) {
            if ($chan_codec eq "$codec") {
                $results[$i] = $results[$i] + 1;
                $found = 1;
                last;
            }
            $i++;
        }
        if (! $found) {
            $other += 1;
            print STDERR "PJSIP other format: $chan_codec\n";
        }
    }
    $start = 1 if ($line =~ /^Output:(\s+)?(=)+$/o);
}

$pop->print("Action: logoff");
$pop->print("");

## Exhaust buffer before closing (to avoid polluting Asterisk's logs)
while ($line = $pop->getline) {}

$i = 0;
foreach my $codec (@CODECS) {
    print "$codec.value $results[$i]\n";
    $i++;
}
print "other.value $other\n";
print "unknown.value $unknown\n";
#print STDERR "asterisk_codecs other.value $other\n";
#print STDERR "asterisk_codecs unknown.value $unknown\n";

# vim:syntax=perl
