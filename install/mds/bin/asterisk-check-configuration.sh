#!/usr/bin/env bash

AST_DP_GLOBALS_PATH="/etc/xivo/asterisk/xivo_globals.conf"

DRIVER_USED=$(cat $AST_DP_GLOBALS_PATH | awk -F= '/^XIVO_SIPDRV/ { print $2 }')

if [ "$DRIVER_USED" = "SIP" ]; then
  command="sip show peers"
  re="[[:space:]]0 sip peers"
else
  command="pjsip show endpoints"
  re="No objects found."
fi

peers=$(/usr/sbin/asterisk -rx "${command}")
if [[ ${peers} =~ $re ]]; then
  exit 1
else
  exit 0
fi
