xivo-monitoring
===============

This package contains configuration and check scripts which are executed by the *monit* package.

It specifies custom actions to be run if the check fails.

It also specifies start and stop actions for buttons on Monitoring page of admin interface.

On MDS it monitors asterisk peers and if they are not synchronized with mds0
(because asterisk started before confgend), it reloads asterisk modules.

The xivo-monitoring-update scripts have two purposes:

- to resolve problems with package uninstallation and monit reload
- to replace configuration file of the monit


Troubleshooting
---------------

The first check can be evaluated as "ok" even if the check script returns exit code 1.

If you run `monit status` right after monit start, you can see this error:

    Cannot create socket to [localhost]:2812 -- Connection refused

It takes more time before monit initialises the socket.
